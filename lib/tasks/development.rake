namespace :development do
  desc "Destrói e repopula todos os dados da apilcação"
  task setup: :environment do
    %x(rails db:drop:_unsafe)
    puts %x(rails db:create)
    puts %x(rails db:migrate)
    puts %x(rails db:migrate RAILS_ENV=test)
    puts %x(rails development:seed)
  end

  desc "Popula a aplicação com dados falsos"
  task seed: :environment do
    puts "[init] Adicionando Tópicos"
    15.times do |i|
      Topic.create!(
        content: Faker::Lorem.sentence(rand(3..8))
      )
    end
    puts "[done] Adicionando Tópicos"

    puts "[init] Adicionando Comentários"
    topics = Topic.all
    topics.each do |topic|
      rand(1..2).times do
        Comment.create!(
          content: Faker::Lorem.sentence(rand(3..8)),
          topic: topic
        )
      end
    end
    puts "[done] Adicionando Comentários"
  end
end
