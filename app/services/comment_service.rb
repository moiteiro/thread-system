class CommentService
  class << self
    def sanitize(comment)
      blacklist = YAML.load_file("#{Rails.root.to_s}/lib/assets/blacklist.yml")

      characters = {
        i: /i|í|ì/i,

        a: /a|ä|@|á|à/i,
        e: /e/i,
        o: /o/i,
        u: /u/i
      }

      blacklist.each do |word|
        word_size = word.size
        characters.each do |char, exp|
          word.gsub!(/#{char}/, "#{exp}")
        end
        comment.content.gsub!(/#{word}/, "*" * (word_size))
      end

      comment
    end
  end
end