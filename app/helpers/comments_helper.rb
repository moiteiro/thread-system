module CommentsHelper

  # Melhorar isso depois. Ta horrível
  def nested_comments(comments)
    current_level = 1
    list = "<ul>"

    comments.each do |comment|
      level = comment.ancestors.split('.').count

      if current_level < level
        current_level += 1
        list += "<ul>"
      elsif current_level > level
        list += "</ul>" * (level - current_level).abs
        current_level = level
      end

      list += render(comment)
    end
    list += "</ul>"
    list.html_safe
  end

  def nested_comment_position(order_code)
    order_code.gsub(/0/, '')
  end
end
