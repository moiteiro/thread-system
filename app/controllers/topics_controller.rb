class TopicsController < ApplicationController
  before_action :set_topics

  def index
    @topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)
    if @topic.save
      redirect_to topics_path, notice: "Tópico criando com sucesso."
    else
      render :index
    end

  end

  private
  def topic_params
    params.require(:topic).permit(:content)
  end

  def set_topics
    @topics = Topic.page params[:page]
  end
end
