class CommentsController < ApplicationController
  before_action :set_topic, only: [:index, :create]

  def index
    set_comments
    @comment = @topic.comments.new
  end

  def create
    @comment = @topic.comments.new(comment_params)

    @comment = CommentService.sanitize(@comment)

    if @comment.save
      redirect_to topic_comments_path(@topic), notice: "Comentário criando com sucesso."
    else
      set_comments
      render :index
    end
  end

  private
  def comment_params
    paramenters = params.require(:comment).permit(:content)
    if params[:comment_id].present?
      paramenters.merge!(comment_id: params[:comment_id])
    end
    paramenters
  end

  def set_topic
    @topic = Topic.find params[:topic_id]
  end

  def set_comments
    @comments = @topic.comments.order_by_ancestors.page params[:page]
  end

end
