class Comment < ApplicationRecord
  after_create :set_ancestors

  belongs_to :topic
  has_many :comments

  validates :content, presence: true

  scope :order_by_ancestors, -> {order(ancestors: :asc)}

  private

  def set_ancestors
    if comment_id.present?
      parent = Comment.find(comment_id)
      children = parent.comments.count.to_s.rjust(5,'0')
      self.ancestors = "#{parent.ancestors}.#{children}"
    else
      count = topic.comments.count
      self.ancestors = count.to_s.rjust(5,'0')
    end
    self.save
  end
end

