require 'rails_helper'

RSpec.feature "Topics", type: :feature do
  context 'Listando Tópicos:' do
    scenario 'consegue acessar a página de tópicos' do
      visit(topics_path)
      expect(page).to have_content('Tópicos')
    end

    scenario 'consegue ver a lista de tópicos' do
      topics = create_list(:topic, 5)
      visit(topics_path)
      expect(page).to have_content(topics.first.content)
    end

    scenario 'lista apenas 10 tópicos por página' do
      topics = create_list(:topic, 15)
      visit(topics_path)
      expect(page).to have_content(topics.first.content)
      expect(page).not_to have_content(topics.last.content)
    end

    scenario 'Consegue trocar de página' do
      topics = create_list(:topic, 15)
      visit(topics_path)
      click_on("Next")
      expect(page).to have_content(topics.last.content)
    end

    scenario 'consegue ver o formulário de criação de tópicos' do
      visit(topics_path)
      expect(page).to have_content('Novo Tópico')
    end
  end

  context 'Criando Tópicos:' do
    scenario 'consegue criar um tópico válido' do
      visit(topics_path)
      topic_content = Faker::Lorem.sentence(rand(3..5))
      fill_in 'topic_content', with: topic_content
      click_on('Create Topic')
      expect(page).to have_content(topic_content)
      expect(page).to have_content('Tópico criando com sucesso.')
    end

    scenario 'não consegue criar um tópico inválido' do
      visit(topics_path)
      click_on('Create Topic')
      expect(page).to have_content('can\'t be blank')
    end
  end
end