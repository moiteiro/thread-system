require 'rails_helper'

RSpec.feature "Comments", type: :feature do

  before(:each) do |test|
    @topic = create(:topic)
    @comment = create(:comment, topic: @topic) unless test.metadata[:clear]
  end

  context 'Listando Comentários:' do
    scenario 'consegue acessar a página de comentários' do
      visit(topics_path)
      first(:link, 'Comment').click
      expect(page).to have_content('Tópico')
    end

    scenario 'consegue acessar o comentário de um tópico' do
      visit(topics_path)
      all(:link, 'Comment').last.click
      expect(page).to have_content('Tópico')
      expect(page).to have_content(@topic.content)
      expect(page).to have_content(@comment.content)
    end

    scenario 'lista apenas 10 comentários por página', :clear do
      comments = create_list(:comment, 15, topic: @topic)
      visit(topic_comments_path(@topic))
      expect(page).to have_content(comments.first.content)
      expect(page).not_to have_content(comments.last.content)
    end

    scenario 'Consegue trocar de página', :clear do
      comments = create_list(:comment, 15, topic: @topic)
      visit(topic_comments_path(@topic))
      click_on("Next")
      expect(page).not_to have_content(comments.first.content)
      expect(page).to have_content(comments.last.content)
    end
  end

  context 'Criando Comentários:', :clear do
    scenario 'consegue criar um comentário válido' do
      visit(topic_comments_path(@topic))
      comment_content = Faker::Lorem.sentence
      fill_in 'comment_content', with: comment_content
      click_on 'Create Comment'
      expect(page).to have_content('Tópico')
      expect(page).to have_content(comment_content)
    end
  end
end