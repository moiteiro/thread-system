require 'rails_helper'

RSpec.describe Comment, type: :model do
  it 'should have a valid content' do
    comment = create(:comment)
    expect(comment).to be_valid
  end

  it 'is invalid without content' do
    comment = build(:comment, content: nil)
    comment.valid?
    expect(comment.errors[:content]).to include("can't be blank")
  end

  it 'is needs to have a topic associated' do
    comment = build(:comment, topic: nil)
    comment.valid?
    expect(comment.errors[:topic]).to include("must exist")
  end

  it 'always have ancestors info set after being created' do
    comment = create(:comment)
    expect(comment.ancestors).not_to be_nil
  end

  it 'inherits parent ancestors' do
    parent = create(:comment)
    comment = create(:comment, comment_id: parent.id)
    expect(comment.ancestors).to include("#{parent.ancestors}")
  end
end
