require 'rails_helper'

RSpec.describe Topic, type: :model do
  it 'should have a valid content' do
    topic = create(:topic)
    expect(topic).to be_valid
  end

  it 'is invalid without content' do
    topic = build(:topic, content: nil)
    topic.valid?
    expect(topic.errors[:content]).to include("can't be blank")
  end

  it 'can have comments' do
    topic = create(:topic)
    comments = create_list(:comment, 5, topic: topic)
    expect(topic.comments.count).to eq(5)
  end
end
