require 'rails_helper'

RSpec.describe CommentService, type: :service do

  before(:all) do
    blacklist = YAML.load_file("#{Rails.root.to_s}/lib/assets/blacklist.yml")

    @strong_word = blacklist.sample
    @censured_word = "*" * @strong_word.size
    @strong_phrase = "#{Faker::Lorem.sentence(rand(3..5))} #{@strong_word}"
    @strong_paragraph = "#{@strong_phrase} #{Faker::Lorem.sentence(rand(3..5))} #{@strong_word}"
  end

  context "With Blacklist file:" do
    it 'can sanitize comments with one strong word' do
      comment = build(:comment, content: @strong_word)
      comment = CommentService.sanitize(comment)
      expect(comment.content).to include("#{@censured_word}")
      expect(comment.content).not_to include("#{@strong_word}")
    end

    it 'can sanitize comments with long content' do
      comment = build(:comment, content: @strong_phrase)
      comment = CommentService.sanitize(comment)
      expect(comment.content).to include("#{@censured_word}")
      expect(comment.content).not_to include("#{@strong_word}")
    end

    it 'can sanitize comments with multiple strong words' do
      comment = build(:comment, content: @strong_paragraph)
      comment = CommentService.sanitize(comment)
      expect(comment.content).to include("#{@censured_word}")
      expect(comment.content).not_to include("#{@strong_word}")
    end
  end
end
