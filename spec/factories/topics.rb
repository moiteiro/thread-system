FactoryBot.define do
  factory :topic do
    content {Faker::Lorem.sentence(rand(3..8))}
  end
end
