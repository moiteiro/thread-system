FactoryBot.define do
  factory :comment do
    content {Faker::Lorem.sentence(rand(3..8))}
    topic
  end
end
